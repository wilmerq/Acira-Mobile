export class Usuario {

    constructor(nombre: String, contrasena: String) {
        this.nombre = nombre
        this.contrasena = contrasena
    }

    nombre: String
    contrasena: String
}