import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaphistorialPage } from './maphistorial';

@NgModule({
  declarations: [
    MaphistorialPage,
  ],
  imports: [
    IonicPageModule.forChild(MaphistorialPage),
  ],
})
export class MaphistorialPageModule {}
