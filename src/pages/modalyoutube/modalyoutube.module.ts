import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalyoutubePage } from './modalyoutube';

@NgModule({
  declarations: [
    ModalyoutubePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalyoutubePage),
  ],
})
export class ModalyoutubePageModule {}
